require 'rails_helper'

RSpec.describe User, type: :model do
  #forma 4 com shoulda matcher
  let(:user) { build(:user) }

  it {is_expected.to validate_presence_of(:email)}
  it {is_expected.to validate_uniqueness_of(:email).case_insensitive}
  it {is_expected.to validate_confirmation_of(:password)}
  it {is_expected.to allow_value("flavio@gmail.com").for(:email)}
  it {is_expected.to validate_uniqueness_of(:auth_token)}

  describe '#info' do
    it 'returns e-mail and created_at' do
      user.save!

      expect(user.info).to eq("#{user.email} - #{user.created_at}")  
    end  
  end  

  #forma 3
  #subject {build(:user)}

  #forma 1
  #before { @user = FactoryGirl.build(:user) }
  
  #verifica se o usuario foi criado
  # it {expect(@user).to respond_to(:email)}
  # it {expect(@user).to respond_to(:name)}
  # it {expect(@user).to respond_to(:password)}
  # it {expect(@user).to respond_to(:password_confirmation)}
  #verifica se o usuário criado é válido
  #it {expect(@user).to be_valid }


  #forma 2
  #it {expect(subject).to respond_to(:email)} 
  #it {expect(subject).to be_valid }


  # it {expect(@user).to respond_to(:email)}
  #it {expect(subject).to respond_to(:email)}


  # it {is_expected.to respond_to(:email)}
  # it {is_expected.to be_valid }
  # it {is_expected.to respond_to(:name)}
end
